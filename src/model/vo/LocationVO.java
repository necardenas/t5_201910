package model.vo;

public class LocationVO implements Comparable<LocationVO>{
	
	private int addressId;
	
	private String location;
	
	private int numbreOfRegisters;

	public LocationVO(int addressId, String location, int numbreOfRegisters) {
		super();
		this.addressId = addressId;
		this.location = location;
		this.numbreOfRegisters = numbreOfRegisters;
	}


	public int getAddressId() {
		return addressId;
	}


	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getNumbreOfRegisters() {
		return numbreOfRegisters;
	}

	public void setNumbreOfRegisters(int numbreOfRegisters) {
		this.numbreOfRegisters = numbreOfRegisters;
	}

	@Override
	public int compareTo(LocationVO o) {
	
		int x = numbreOfRegisters - o.numbreOfRegisters;		
		if (x == 0 ) {
			x = location.compareTo(o.location);
		}
		return x;
	}
}
