package model.data_structures;

public class Node<E> 
{
	public E item;
	
	public Node<E> next;

	public Node(E item, Node<E> next) 
	{
		this.item = item;
		this.next = next;
	}
	
	public Node (E item)
	{
		this.item = item;
		this.next = null;
	}

	public E getItem() {
		return item;
	}

	public void setItem(E item) {
		this.item = item;
	}

	public Node<E> getNext() {
		return next;
	}

	public void setNext(Node<E> next) {
		this.next = next;
	}
	
	
}
