package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import model.data_structures.ArregloDinamico;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.util.Sort;
import model.vo.LocationVO;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;
import view.VOViolationCode;

public class Controller 
{

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;


	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> QueueCopy;


	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;


	public Controller() 
	{
		view = new MovingViolationsManagerView();

		//TODO, inicializar la pila y la cola
		movingViolationsQueue = new Queue<VOMovingViolations>();
		movingViolationsStack = new Stack<VOMovingViolations>();
	}

	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean loaded = false;
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1: //CARGAR
				if (!loaded)
				{
					this.loadMovingViolations();
					System.out.println("Los datos fueron cargados");
					view.printMovingViolations(movingViolationsStack);
					loaded = true;
					break;
				}
				else
				{
					System.out.println("Los datos ya fueron cargados (Solo se pueden cargar una vez)");
					System.out.println();
					break;
				}


			case 0:	
				fin=true;
				sc.close();
				break;
			}
		}
	}


	/**
	 * Carga los datos de un cuatrimestre dado
	 * post: Se cargaron los datos del cuatrimestre
	 * pre: La cola fue inicializada
	 * @param cuatrimestre
	 */
	public void loadMovingViolations() 
	{
		try
		{
			String path = ".\\data\\Moving_Violations_Issued_in_";
			String mth1Path = path + "January_2018.csv";
			String mth2Path = path + "February_2018.csv";
			String mth3Path = path + "March_2018.csv";
			String mth4Path = path + "April_2018.csv";

			String[] mths = {mth1Path, mth2Path, mth3Path, mth4Path};
			String cPath = "";
			CSVReader reader = null;

			int i = 0;
			while (i < mths.length)
			{
				cPath = mths[i];
				reader = new CSVReaderBuilder(new BufferedReader(new FileReader(new File(cPath)))).withSkipLines(1).build();

				String[] rows = null;

				while ((rows = reader.readNext()) != null)
				{
					String in1 = rows[0];
					Integer objId = Integer.parseInt(in1);

					String location = rows[2];

					String in2 = rows[3];
					int adressId = 0;
					if ( !(in2.compareTo("") == 0) )
						adressId = Integer.parseInt(in2);

					String in3 = rows[4];
					double streetId = 0;
					if ( !(in3.compareTo("") == 0) )
						streetId = Double.parseDouble(in3);

					String in4 = rows[5];
					Double xCord = Double.parseDouble(in4);

					String in5 = rows[6];
					Double yCord = Double.parseDouble(in5);

					String ticketType = rows[7];

					String in6 = rows[8];
					Integer fineAmt = Integer.parseInt(in6);

					String in7 = rows[9];
					double totalPaid = Double.parseDouble(in7);

					String in8 = rows[10];
					Integer penalty1 = 	Integer.parseInt(in8);

					String in9=rows[11];
					Integer penalty2=Integer.parseInt(in9);

					String accident = rows[12];

					String date = rows[13];

					String vioCode = rows[14];

					String vioDesc = rows[15];

					VOMovingViolations vomv = new VOMovingViolations(objId, location, adressId, streetId, xCord, yCord, ticketType, fineAmt, totalPaid, penalty1, penalty2, accident, date, vioCode, vioDesc);
					movingViolationsQueue.enqueue(vomv);
					movingViolationsStack.push(vomv);
				}
				i++;
				reader.close();
			}

		}



		catch (Exception e)
		{
			e.printStackTrace();
		}

	}


	public IQueue<VOMovingViolations> generateRandomSample(int sampleSize) {
		QueueCopy = movingViolationsQueue;
		ArregloDinamico<VOMovingViolations> list = new ArregloDinamico<>(QueueCopy.size());
		List<Integer> random = new ArrayList<>(QueueCopy.size());

		for(int i =0; i < movingViolationsQueue.size(); i++) {			
			list.agregar(QueueCopy.delMax());
			random.add(i);
		}

		IQueue<VOMovingViolations> sampleList = new Queue<>();

		while (sampleSize>1){
			// Elegimos un índice al azar, entre 0 y el número de elementos que quedan por sacar
			int randomIndex = ThreadLocalRandom.current().nextInt(0, random.size() + 1);

			random.remove(randomIndex);
			sampleList.enqueue(list.darElemento(randomIndex));

			// Y eliminamos el numero de la lista)
			random.remove(randomIndex);
			sampleSize--;

		}
		return sampleList;
	}

	public void graphic() {

		System.out.println("Tiempo de para Cargar en la cola de prioridad tam | tiempo");


		for(int i = 50000; i< 350000;i=i+50000 ) {
			QueueCopy = movingViolationsQueue;
			long startTime = System.currentTimeMillis();
			this.addQueueTime(QueueCopy, i);
			long endTime = System.currentTimeMillis();
			long duration = endTime - startTime;
			System.out.println(i+ ":" + duration + " milisegundos");
		}

		System.out.println("Tiempo de para eliminar en la cola de prioridad tam | tiempo");

		for(int i = 50000; i< 350000;i=i+50000 ) {
			QueueCopy = movingViolationsQueue;
			long startTime = System.currentTimeMillis();
			this.deleteQueueTime(QueueCopy, i);
			long endTime = System.currentTimeMillis();
			long duration = endTime - startTime;
			System.out.println(i+ ":" + duration + " milisegundos");
		}

		System.out.println("Tiempo de para eliminar en Heap tam | tiempo");
		for(int i = 50000; i< 350000;i=i+50000 ) {
			QueueCopy = movingViolationsQueue;
			long startTime = System.currentTimeMillis();
			this.deleteHeapTime(QueueCopy, i);
			long endTime = System.currentTimeMillis();
			long duration = endTime - startTime;
			System.out.println(i+ ":" + duration + " milisegundos");
		}

		System.out.println("Tiempo de para agregar en Heap tam | tiempo");
		for(int i = 50000; i< 350000;i=i+50000 ) {
			QueueCopy = movingViolationsQueue;
			long startTime = System.currentTimeMillis();
			this.deleteHeapTime(QueueCopy, i);
			long endTime = System.currentTimeMillis();
			long duration = endTime - startTime;
			System.out.println(i+ ":" + duration + " milisegundos");
		}
	}

	public void addQueueTime(IQueue<VOMovingViolations> queue, int times) {
		IQueue<VOMovingViolations> queue2 = new Queue<>();

		for(int i = 0; i < times ; i++) {
			queue2.enqueue(queue.delMax());
		}

	}

	public void deleteQueueTime(IQueue<VOMovingViolations> queue, int times) {
		while(times>1) {
			queue.delMax();
			times--;
		}
	}

	public void deleteHeapTime(IQueue<VOMovingViolations> queue, int times) {
		while(times>1) {

			times--;
		}
	}

	public void addHeapTime(IQueue<VOMovingViolations> queue, int times) {
		IQueue<VOMovingViolations> queue2 = new Queue<>();

		for(int i = 0; i < times ; i++) {
		}
	}

	public void screarMaxColaP(int num, String date1, String date2) {
		QueueCopy = movingViolationsQueue;
		IQueue<LocationVO> list = new Queue<>();

		for(int i =0; i < movingViolationsQueue.size(); i++) {			
			VOMovingViolations element = QueueCopy.delMax();
			if(element.getTicketIssueDate().compareTo(date1)> 0 && element.getTicketIssueDate().compareTo(date2)< 0 ){
				LocationVO elementToAdd =  new LocationVO(element.getAdress(), element.getLocation(), 0);
				if(list.isEmpty()) {
					list.enqueue(elementToAdd);
				}
				for (LocationVO v: list) {
					if(v.getAddressId() == elementToAdd.getAddressId()) {
						v.setNumbreOfRegisters(v.getNumbreOfRegisters()+1);
					}else {
						list.enqueue(elementToAdd);
					}
				}
			}
			for (int x =0; x< num; x++) {
				LocationVO e = list.delMax();
				System.out.println("El numero de accidentes en la calle "+e.getAddressId()+" fue de: "+ e.getNumbreOfRegisters());
			}
		}
	}


	public void caddressWithMoreViolations(int num, String date1, String date2 ){

		QueueCopy = movingViolationsQueue;
		long startTime = System.currentTimeMillis();
		this.screarMaxColaP(num, date1, date2);
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		System.out.println("La cola se domoro: " + duration + " milisegundos");
	
	}


}



