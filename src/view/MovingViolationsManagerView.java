package view;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.vo.VOMovingViolations;
	
public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() 
	{
		
	}
	
	public void printMenu() 
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
			//LOAD
		System.out.println("1. Cargar datos por cuatrimestre (1 - 4)");
			//Parte A
		System.out.println("2. Verificar que ObjectID es �nico");
		System.out.println("3. Consultar infracciones por fecha y hora inicial y final");
		System.out.println("4. Dar FINEAMT promedio cuando hubo accidente por tipo de infracci�n (VIOLATIONCODE)");
			//Parte B
		System.out.println("5. Consultar FINEAMT promedio por cada tipo de infracci�n (VIOLATIONCODE)");
		System.out.println("6. Consultar infracciones por rango de cantidad pagada (TOTALPAID)");
		System.out.println("7. Consultar infracciones por hora inicial y final");
		System.out.println("8. Dar FINEAMT promedio y la desviaci�n est�ndar por tipo de infracci�n (VIOLATIONCODE) espec�fica");
		System.out.println("9. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	
	
	public void printMovingViolations(IStack<VOMovingViolations> violations) 
	{
		System.out.println("Se encontraron "+ violations.size() + " elementos");
		for (VOMovingViolations violation : violations) 
		{
			System.out.println(violation.getobjectId() + " " 
								+ violation.getTicketIssueDate() + " " 
								+ violation.getLocation()+ " " 
								+ violation.getViolationDescription());
		}
	}
	
	public void printMensage(String mensaje) 
	{
		System.out.println(mensaje);
	}
}
